package forms;


import controls.Button;
import controls.Popup;
import controls.TextBox;
import driver.BaseDriver;
import org.openqa.selenium.By;

/*
* Add here Web elements of current page with methods to access them
* */
public class HomeForm {

  private Button magnifier_btn;
  private TextBox search_txtb;
  private Button game_icn;
  private Button more_btn;
  private Button playNow_btn;
  private Popup login_p;

  public HomeForm(BaseDriver driver) {
    magnifier_btn = new Button(driver, By.xpath("//i[@class='fa fa-search']"));
    search_txtb = new TextBox(driver, By.xpath("//input[@placeholder = 'Search for games...']"));
    game_icn = new Button(driver, By.xpath("(//div[@class='gc-tile__left'])[1]"));
    more_btn = new Button(driver, By.xpath("//span[contains(text(),'More ')]"));
    playNow_btn = new Button(driver, By.xpath("//button[contains(text(),'Play Now')]"));
    login_p = new Popup(driver, By.xpath("//div[@class='login-component__wrapper']"));
  }

  public Button getMagnifier_btn() {
    return magnifier_btn;
  }

  public TextBox getSearch_txtb() {
    return search_txtb;
  }

  public Button getGame_icn() {
    return game_icn;
  }

  public Button getMore_btn() {
    return more_btn;
  }

  public Button getPlayNow_btn() {
    return playNow_btn;
  }

  public Popup getLogin_p() {
    return login_p;
  }
}
