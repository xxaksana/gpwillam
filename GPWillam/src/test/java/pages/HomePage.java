package pages;

import driver.BaseDriver;
import forms.HomeForm;

/*
* Add here methods for current page
* */
public class HomePage extends BasePage {

  private static final String TITLE = "Play Online Casino Games at William Hill Vegas";
  HomeForm homeForm;

  public HomePage(BaseDriver driver) {
    super(driver, TITLE);
    homeForm = new HomeForm(driver);
  }

  public HomePage searchForCompany(String game) {
    homeForm.getMagnifier_btn().click();
    homeForm.getSearch_txtb().setText(game);
    homeForm.getGame_icn().hover();
    homeForm.getMore_btn().click();
    homeForm.getPlayNow_btn().click();
    return this;
  }

  public boolean isLoginFormDisplayed() {
    return homeForm.getLogin_p().isDisplayed();
  }
}
