package testSuit;

import driver.BaseDriver;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

/*
* Initializing webdriver and @before @after test
* */
public class BaseTest {

  static BaseDriver driver;

  @BeforeTest
  public void intiDriver() {
    DesiredCapabilities capabilities = DesiredCapabilities.chrome();
    ChromeOptions options = new ChromeOptions();
    options.addArguments("--start-maximized");
    Map<String, Object> prefs = new HashMap<String, Object>();
    prefs.put("credentials_enable_service", false);
    prefs.put("profile.password_manager_enabled", false);
    options.setExperimentalOption("prefs", prefs);
    capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
    capabilities.setCapability("chrome.switches", Arrays.asList("--incognito"));
    capabilities.setCapability(ChromeOptions.CAPABILITY, options);
    capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
        UnexpectedAlertBehaviour.IGNORE);
    driver = new BaseDriver();
    driver.setDriver(new ChromeDriver(capabilities));
  }

  @AfterTest
  public void closeDriver() {
    driver.quitDriver();
    System.out.println("stop");
  }
}
