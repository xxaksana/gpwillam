package testSuit;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;

/*
* “As a WH Customer

I want to be able to search for specific game on Vegas Casino and login to the webpage”

Navigate to https://vegas.williamhill.com/

In the top-right corner click magnifier button

Search for „Mayfair Roulette” game

Hover over the „Mayfair Roulette” game tile and click „More” button

Click „Play Now” and validate full Login Window is displayed

Note: Game name (Mayfair Roulette) should a parameter so it can be easily changed during
script execution
* */
public class SearchingSpecificGameOnVegasCasino extends BaseTest {

  @Test
  public void verifyManualReconnectionInformation() {
    driver.navigateTo("https://vegas.williamhill.com/");
    Assert.assertTrue(
        new HomePage(driver).searchForCompany("Mayfair Roulette").isLoginFormDisplayed());
  }
}


