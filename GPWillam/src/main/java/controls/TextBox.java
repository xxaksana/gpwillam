package controls;

import driver.BaseDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
/*
* Add here all actions for a textbox
* */

public class TextBox extends WebControl {

  public TextBox(BaseDriver driver, By locator) {
    super(driver, locator);
  }

  public TextBox setText(String text) {
    WebElement e = getElement();

    e.clear();
    driver.setTextWithTimeout(e, text, locator);
    return this;
  }
}
