package controls;

import driver.BaseDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/*
* Add here all actions for a popup
* */

public class Popup extends WebControl {

  public Popup(BaseDriver driver, By locator) {
    super(driver, locator);
  }

  public boolean isDisplayed() {
    WebElement e = getElement();
    return e.isDisplayed();
  }
}
