package controls;

import driver.BaseDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/*
* Add here all actions for a button
* */
public class Button extends WebControl {

  public Button(BaseDriver driver, By locator) {
    super(driver, locator);
  }

  public void click() {
    WebElement e = getElement();
    e.click();
  }

  public void hover() {
    WebElement e = getElement();
    driver.mouseOver(e);
  }
}
