package pages;

import driver.BaseDriver;

/*
* In this class I'm checking title of page with a wait
* */
public class BasePage {

  public BaseDriver driver;
  public String title;

  public BasePage(BaseDriver driver, String title) {
    this.driver = driver;
    this.title = title;
    driver.waitTitle(this.title);
    if (!driver.getTitle().contains(this.title)) {
      throw new IllegalStateException("This is not correct page");
    }
  }
}
