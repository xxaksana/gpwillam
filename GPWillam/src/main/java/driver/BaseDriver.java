package driver;

import static com.sun.javafx.font.PrismFontFactory.isAndroid;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by xxaksana on 17/05/2018.
 */
public class BaseDriver {
  /*
* Add global methods to use in for different elements
* */

  private static WebDriver driver;

  public String getTitle() {
    return driver.getTitle();
  }

  public WebElement getElement(By locator) {
    return driver.findElement(locator);
  }

  public void setDriver(WebDriver driver) {
    BaseDriver.driver = driver;
  }

  public static WebDriver getDriver() {
    return driver;
  }

  public static void quitDriver() {
    driver.quit();
  }

  public void navigateTo(String url) {
    driver.navigate().to(url);
  }

  public void mouseOver(WebElement element) {
    Actions actObj = new Actions(getDriver());
    actObj.moveToElement(element).build().perform();
  }

  public void waitTitle(String title) {
    if (isAndroid) {
      WebDriverWait wait = new WebDriverWait(driver, 15);
      wait.until(ExpectedConditions.titleContains(title));
    } else {
      WebDriverWait wait = new WebDriverWait(driver, 15);
      wait.until(ExpectedConditions.titleContains(title));
    }
  }

  public void wait(By locator) {
    try {
      WebDriverWait wait = new WebDriverWait(driver, 30);
      wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    } catch (UnhandledAlertException u) {
      driver.switchTo().alert().accept();
    }
  }

  public void setTextWithTimeout(WebElement element, String text, By locator) {
    try {
      WebDriverWait wait = new WebDriverWait(driver, 5);
      wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
      element.clear();
      element.sendKeys(text);
    } catch (StaleElementReferenceException e) {
    }
  }
}
